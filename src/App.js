import React from 'react';
import RoadManagement from "./components/roadmanagement/RoadManagement";

function App() {
  return (
    <div className="App">
      <RoadManagement/>
    </div>
  );
}

export default App;
